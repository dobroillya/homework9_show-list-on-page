function showList(arr, el = document.body) {
  const ul = document.createElement("ul");
  for (let item of arr) {
    ul.innerHTML += `<li>${item}</li>`
  }
  el.append(ul);
}

showList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);

showList(["1", "2", "3", "sea", "user", 23]);
